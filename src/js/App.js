import React from 'react';
import ToolBar from './components/ToolBar/ToolBar';
import UserList from './components/UserList/UserList';
import Header from "./components/Header/Header";


export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [],
            searchData: [],
            selectboxOptions: ["Только разработчики", "Дизайнеры", "Фронтенд", "Бэкенд", "Android"],
            experienceDirection: true,
            viewType: "list__users-table_view",
            selectboxActive: "active",
            selectboxTitle: "Показать всех",
            cardActive: "",
            tableActive: "active",
            sortNameActive: "",
            sortExpActive: "",
            secondNameDirection: true
        };
        this.getData('./data.json');
    }

    getData(data) {
        fetch(data)
            .then((response) => {
                return response.json()
            })
            .then((json) => this.setState({
                data: json,
                searchData: json
            }))
            .catch((error) => console.log(error))
    }

    selectUser(id) {
        this.setState({activeUser: id});
    }

    changeToTable() {
        let tooggler = this.state.tableActive;
        if (tooggler != "active") {
            this.setState({
                viewType: "list__users-table_view",
                cardActive: "",
                tableActive: "active"
            });
        }
    }

    changeToCard() {
        let tooggler = this.state.cardActive;
        if (tooggler != "active") {
            this.setState({
                viewType: "list__users-card_view",
                cardActive: "active",
                tableActive: ""
            });
        }
    }

    sortByExp() {
        let data = this.state.data;
        let direction = this.state.experienceDirection;

        this.setState({
            data: direction ? data.sort((a, b) => a.experience - b.experience) : data.sort((a, b) => b.experience - a.experience),
            experienceDirection: !direction,
            activeUser: data[0].id,
            sortNameActive: "",
            sortExpActive: "active"
        });
    }

    sortByName() {
        let data = this.state.data;
        let direction = this.state.secondNameDirection;
        let sortedData = data.sort(direction ? (a, b) => a.name > b.name ? 1 : -1 : (a, b) => a.name < b.name ? 1 : -1);

        this.setState({
            data: sortedData,
            secondNameDirection: !direction,
            activeUser: data[0].id,
            sortNameActive: "active",
            sortExpActive: ""
        });
    }


    searchBy(str) {
        const searchingData = this.state.searchData.filter((item) => {
            return item.profession.toLowerCase().indexOf(str.toLowerCase()) !== -1;
        });
        const userId = searchingData.length ? searchingData[0].id : false;
        this.setState({
            data: searchingData,
            activeUser: userId
        })
    }

    render() {
        return (
            <div className="wrapper">
                {console.log()}
                <section className="section header">
                    <Header/>
                </section>
                <section className="section controls">
                    <ToolBar searchBy={this.searchBy.bind(this)} sortNameActive={this.state.sortNameActive}
                             sortExpActive={this.state.sortExpActive} tableIcon={this.state.tableActive}
                             cardIcon={this.state.cardActive} selectStatus={this.state.selectboxActive}
                             selectboxTitle={this.state.selectboxTitle} selectboxOptions={this.state.selectboxOptions}
                             changeToTable={this.changeToTable.bind(this)} changeToCard={this.changeToCard.bind(this)}
                             sortByExp={this.sortByExp.bind(this)} sortByName={this.sortByName.bind(this)}/>
                    <hr/>
                </section>
                <section className="section list">
                    <UserList data={this.state.data} viewType={this.state.viewType}/>
                </section>
            </div>
        )
    }
}
