import React from 'react';

export default class UserData extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        function plural(n, str1, str2, str5) {
            return n + ' ' + ((((n % 10) == 1) && ((n % 100) != 11)) ? (str1) : (((((n % 10) >= 2) && ((n % 10) <= 4)) && (((n % 100) < 10) || ((n % 100) >= 20))) ? (str2) : (str5)))
        }
        function plurMonths(days) {
            let monthCount = days / 30;
            console.log(monthCount);
            if (monthCount < 12) {
                let monthPlural = plural(monthCount, 'месяц', 'месяца', 'месяцев');
                return ' ' + monthPlural;
            }
            else if (monthCount >= 12) {
                monthCount = monthCount - Math.floor(monthCount/12)*12;
                if (monthCount != 0) {
                    let monthPlural = plural(monthCount, 'месяц', 'месяца', 'месяцев');
                    return ' и ' + monthPlural;
                }
                else {
                    return '';
                }
            }
        }
        function plurYears(days) {
            let yearCount = Math.floor(days / 360);
            if (yearCount > 1) {
                let yearPlural = plural(yearCount, 'год', 'года', 'лет');
                return yearPlural;
            }
            else if (yearCount == 1) {
                return 'Год'
            }
            else {
                return '';
            }
        }
        const age = plural(this.props.data.age, 'год', 'года', 'лет');
        const experience = plurYears(this.props.data.experience) + plurMonths(this.props.data.experience);
        return (
            <div className="users__item">
                <img src={`images/content/${this.props.data.image}.png`} alt="" className="user__item-img"/>
                <div className="user__item-name">{this.props.data.name}</div>
                <div className="user__item-profession">{this.props.data.profession}</div>
                <div className="user__item-age">{age}</div>
                <div className="user__item-experience">{experience}</div>
            </div>
        )
    }
}