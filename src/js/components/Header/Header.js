import React from 'react';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header__title-block">
                <div className="title__name">Отдел разработки</div>
                <div className="title__quantity">6 человек</div>
            </div>
        )
    }
}