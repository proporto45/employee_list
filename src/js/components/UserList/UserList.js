import React from 'react';
import UserData from '../UserData/UserData'

export default class UserList extends React.Component {
    constructor(props) {
        super(props);

    }

    selectUser(id) {
        this.props.selectUser(id);
    }

    render() {
        const data = this.props.data;
        const listType = `list__users + ${this.props.viewType}`;
        const content = data.map((item) => {
            return <UserData selectUser={this.selectUser.bind(this)} data={item} key={item.id}/>
        });
        return (
            <div className={listType}>
                {content}
            </div>
        )
    }
}