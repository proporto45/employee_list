import React from 'react';

export default class ToolBar extends React.Component {
    constructor(props) {
        super(props);

    }
    searchBy(str) {
        this.props.searchBy(str.target.value);
    }
    render() {
        const cardClass = `switch-card_icon ${this.props.cardIcon}`;
        const tableClass = `switch-table_icon ${this.props.tableIcon}`;
        const sortExpClass = `button__sort-by_exp ${this.props.sortExpActive}`;
        const sortNameClass = `button__sort-by_exp ${this.props.sortNameActive}`;
        return (
            <div className="controls__button-block">
                <div className="button__sort">
                    <h3>Сортировать</h3>
                    <div className={sortExpClass} onClick={this.props.sortByExp.bind(this)}>по опыту</div>
                    <div className={sortNameClass} onClick={this.props.sortByName.bind(this)}>по фамилии</div>
                </div>
                <div className="button__filter">
                    <select name="professions" id="" className="button__filter-profession" onChange={this.searchBy.bind(this)}>
                        <option value="" default>Показывать всех</option>
                        <option value="Разработчик">Только разработчики</option>
                        <option value="Дизайнер">Дизайнеры</option>
                        <option value="Фронтенд">Фронтенд</option>
                        <option value="Бэкенд">Бэкенд</option>
                        <option value="Android">Android</option>
                        <option value="Ios">Ios</option>
                    </select>
                </div>
                <div className="button__switch">
                    <div className={cardClass} onClick={this.props.changeToCard.bind(this)}></div>
                    <div className={tableClass} onClick={this.props.changeToTable.bind(this)}></div>
                </div>
            </div>
        )
    }
}