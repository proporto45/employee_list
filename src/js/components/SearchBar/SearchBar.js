import React from 'react';

export default class SearchBar extends React.Component {
    constructor() {
        super();

    }
    showOnly(str) {
        alert(str);
        this.props.searchBy(str.target.value);
    }

    render() {
        return(
            <div className="search-bar container-fluid col s12">
                <input onChange={this.showOnly.bind(this)} type="text" className="z-depth-1"/>
            </div>
        )
    }
}
